import 'package:tetris/tetris.dart';
import 'dart:html';
import 'package:tetris/src/controller/api.dart';

class View{

  static String username = '';
  static String userID = '';
  static int scors = 0;


  void start(){
    loginOrRegister();
  }


  void loginOrRegister(){
    final FormElement? loginForm = querySelector('#login-form') as FormElement?;
    final FormElement? registerForm = querySelector('#register-form') as FormElement?;
    final DivElement resultDiv = querySelector('#result') as DivElement;

    if (loginForm == null || registerForm == null) {
      print('Forms not found.');
      return;
    }

    //Listener for Login Form submit
    loginForm.onSubmit.listen((event) async {
      event.preventDefault(); // Prevent form submission

      final formData = FormData(loginForm);
      final email = formData.get('login-email') as String?;
      final password = formData.get('login-password') as String?;

      if (email == null || password == null) {
        resultDiv.text = 'Please enter both email and password.';
        return;
      }

      try {
        final userData = await loginUser(email, password); // invoke API for login return all user informtion
        username = userData['name'];
        userID = userData['id'];
        scors = int.parse(userData['scors']);

        handleSuccessRegistration();

        } catch (error) {
          resultDiv.text = 'Login failed. Please check your email and password.';
        }
    });

    //Listener for Reister Form submit
    registerForm.onSubmit.listen((event) async {
      event.preventDefault(); // Prevent form submission

      final formData = FormData(registerForm);
      final name = formData.get('register-name') as String?;
      final email = formData.get('register-email') as String?;
      final password = formData.get('register-password') as String?;

      if (name == null || email == null || password == null) {
        resultDiv.text = 'Please fill in all registration fields.';
        return;
      }

      try {
        userID = await registerUser(name, email, password);  // invoke API for register return user ID
        username = name;

        handleSuccessRegistration();
        
      } catch (error) {
        resultDiv.text = 'Registration failed. Please try again later.';
      }
    });

  } // End loginOrRegister method.


  void handleSuccessRegistration(){
    final registrationDiv = querySelector('#form-container')!;
    registrationDiv.style.display = 'none';
    
    chooesLevel();
  }


  void chooesLevel() {

    Element? entryPoint = querySelector('#level');
    DivElement radioDiv = DivElement()..className = 'radio-container';

    // Create radio buttons for different levels
    String levels = 'Easy, Medium, Hard'; 
    List<String> levelList = levels.split(', ');
    for (String level in levelList) {
      RadioButtonInputElement radioButton = RadioButtonInputElement()
        ..name = 'level'
        ..value = level
        ..id = level.toLowerCase()
        ..onChange.listen((event) { // Listener for print selected value
          
          String selectedLevel = (querySelector('input[name="level"]:checked') as RadioButtonInputElement).value!;
          
          print('Selected Level: $selectedLevel');
        });

      // Create a label for the radio button
      LabelElement label = LabelElement()
        ..text = level
        ..htmlFor = level.toLowerCase();
      
      radioDiv.append(radioButton);
      radioDiv.append(label);
      radioDiv.append(BRElement());
    } // end for loop.

    // Create a submit button
    ButtonElement submitButton = ButtonElement()
      ..text = 'Start Game'
      ..onClick.listen((event) {
        String selectedLevel = (querySelector('input[name="level"]:checked') as RadioButtonInputElement).value!;
        
        // Start game with Level selected
        Game().start(selectedLevel);
      });

    
    radioDiv.append(submitButton);
    entryPoint?.append(radioDiv);

    // Set one of radio buttons as selected by default
    if(levelList.isNotEmpty){
      RadioButtonInputElement defaultRadioButton = querySelector('#${levelList[0].toLowerCase()}') as RadioButtonInputElement;
      defaultRadioButton.checked = true;
    }

  } // End chooesLevel method.




  static void endTheGame(int currentScors) async {

    if(currentScors > scors){
      try{
        await updateUserScors(userID, currentScors); // invoke API for Update user Scors and print the response.
        scors = currentScors;
      }catch(e){
        print('Error updating scores: $e');
      }
    }
    
    ButtonElement? deleteAccountBtn = querySelector('.delete-account-btn') as ButtonElement?;
    ButtonElement? resetAccountBtn = querySelector('.reset-account-btn') as ButtonElement?;
    ButtonElement? restartGameBtn = querySelector('.restart-game-btn') as ButtonElement?;

    if(deleteAccountBtn == null){print('deleteAccountBtn = null');}
    if(resetAccountBtn == null){print('deleteAccountBtn = null');}
    if(restartGameBtn == null){print('deleteAccountBtn = null');}

    if (deleteAccountBtn != null) {
      
      deleteAccountBtn.onClick.listen((event) async { // Listener for Delete Account button
        
        try{
          await deleteUser(userID);  // invoke API for Delete user and reuten boolean value.
          DivElement parentDiv = querySelector('.score-container') as DivElement;
          parentDiv.innerHtml = '<h2>Your account has been deleted successfully!</h2>';
        }catch(e){
          print('Error updating scores: $e');
        }
        
      });
    }

    if (resetAccountBtn != null) {
      
      resetAccountBtn.onClick.listen((event) async {
        
        try{
          await updateUserScors(userID, 0); // invoke API for Update user Scors and print response.
          DivElement parentDiv = querySelector('.score-container') as DivElement;
          parentDiv.innerHtml = '<h2>Your account has been updated successfully!</h2>';
        }catch(e){
          print('Error updating scores: $e');
        }
        
      });
    }

    if (restartGameBtn != null) {
      
      restartGameBtn.onClick.listen((event) async { // Listener for Restart Game button 
        
        querySelector('.score-container')!.style.display = 'none';
        querySelector('#level')!.style.display = 'block';

      });
    }

  } // End endTheGame method.


}