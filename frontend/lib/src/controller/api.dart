import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:html';

//---------------------- Login endpoint ---------------------------
Future<Map<String, dynamic>> loginUser(String email, String password) async {
  final response = await http.post(
    Uri.parse('http://localhost:5000/login'),
    headers: {'Content-Type': 'application/json'},
    body: json.encode({"email": email, "pwd": password}),
  );

  if (response.statusCode == 200) {
    // Login successful, return user data
    return json.decode(response.body);
  } else {
    // Login failed
    throw Exception('Login failed');
  }
}


//------------------------- get all users endpoint -------------------------
Future<List<Map<String, dynamic>>> fetchUsers() async {
  final response = await http.get(Uri.parse('http://localhost:5000/get_all_users'));

  if (response.statusCode == 200) {
    // Successfully fetched users
    final usersList = json.decode(response.body) as List<dynamic>;
    return usersList.map((user) => user as Map<String, dynamic>).toList();
  } else {
    // Failed to fetch users
    throw Exception('Failed to fetch users');
  }
}

//------------------------- Register new user----------------------
Future<String> registerUser(String name, String email, String password) async {
  final url = 'http://localhost:5000/add_user';
  final headers = {'Content-Type': 'application/json'};
  final body = json.encode({'name': name, 'email': email, 'pwd': password, 'scors': '0'});

  final response = await http.post(Uri.parse(url), headers: headers, body: body);

  if (response.statusCode == 200) {
    // Successfully registration return user ID
    final responseData = json.decode(response.body);
    final userID = responseData['user_id'] as String; // Extract the user ID from the response
    return userID;
  } else {
    // Failed registration
    throw Exception('Registration failed');
  }
}

//------------------------- update user scors endpoint -------------------------
Future<void> updateUserScors(String id, int scors) async {

  try {
    final response = await HttpRequest.request(
      'http://localhost:5000/users/$id',
      method: 'PUT',
      sendData: '{"scors": "$scors"}', // Send JSON data in the request body
      requestHeaders: {'Content-Type': 'application/json'}, // Set content type to JSON
    );

    // print the response if success
    print('Scores updated successfully: ${response.responseText}');
  } catch (e) {
    // Failed
    print('Error updating scores: $e');
  }

}

//----------------------------- delete user ----------------------------
Future<bool> deleteUser(String userId) async {
  try {
    // the URL for the delete_user endpoint
    final String url = 'http://localhost:5000/users/$userId';

    // Make the HTTP DELETE request
    final response = await http.delete(Uri.parse(url));

    if (response.statusCode == 200) {
      // User deleted successfully
      final Map<String, dynamic> data = jsonDecode(response.body);
      print(data['message']);
      return true;
    } else if (response.statusCode == 404) {
      // User not found
      final Map<String, dynamic> data = jsonDecode(response.body);
      print(data['message']);
      return false;
    } else {
      // Handle other error cases
      print('Unexpected error occurred: ${response.statusCode}');
      return false;
    }
  } catch (e) {
    // Failed
    print('Error occurred while deleting user: $e');
    return false;
  }
}