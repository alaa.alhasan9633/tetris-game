
part of tetris;


class Game {

  late CanvasElement board;
  late Element gameScore;
  late Element userName;
  late Block currentBlock;

  static int width = 10;
  static int height = 20;
  static int cellSize = 30;

  static int? linesCleared;
  static int? currentScors;
  static CanvasRenderingContext2D? ctx;

  static List<List<int>>? boardState;
  static List<int>? rowState;

  late String level;
  late Timer playingTimer;
  late int playingTime;

  Game() {
    linesCleared = 0;
    currentScors = 0;
    gameScore = Element.div()..id = "score";
    userName = Element.div()..id = "name";

    rowState = List<int>.filled(height, 0);
    boardState = List.generate(width, (_) => List<int>.filled(height, 0));
  }

  Block getRandomPiece() {
    int randomInt = Random().nextInt(7);
    switch (randomInt) {
      case 0:
        return IBlock(width);
      case 1:
        return OBlock(width);
      case 2:
        return JBlock(width);
      case 3:
        return TBlock(width);
      case 4:
        return LBlock(width);
      case 5:
        return ZBlock(width);
      case 6:
        return SBlock(width);
    }
    return Block();
  } // End of getRandomPiece method.

  void clearRows() {
    // rowState.length is the heigh of main board, so it has 20 row,
    // we loop on this 20 row and check if the row is full then delete it.
    for (int idx = 0; idx < rowState!.length; idx++) {
      int row = rowState![idx];

      if (row == width) { // remove the row, and move down all above pices one step

        // take a snapshot of the board it take parameters: (0,0 start from the center top left, width and heigh)
        ImageData imageData = ctx!.getImageData(0, 0, cellSize * width, cellSize * idx);

        // to move down the acutal data, putImageData it take(the last snapshot, changing on X, changing on Y)
        ctx!.putImageData(imageData, 0, cellSize);

        // for every point should replace its cordinators (x,y) by the point above it.
        for (int y = idx; y > 0; y--) { // for all rows from start to the FULL row
          for (int x = 0; x < width; x++) { // for all columns
            boardState![x][y] = boardState![x][y - 1]; // replace cordinators
          }
          rowState![y] = rowState![y - 1]; // move down
        }

        // set frirt row to be Zeros manually
        rowState![0] = 0;
        boardState!.forEach((c) => c[0] = 0);

        // set Scors
        linesCleared = linesCleared !+ 1;
        
        // Calculate Scores
        calculateScors();
      }
    }
  } // End of clearRows method.

  //Example:
  //[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]       === row state at starting the game
  //[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,7,4,10]      === row state at some point, the buttom line has state 10 (the width) so it is Full
  //[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,7,4]       === row state after moveing down

  //      0        1        2        3
  // [[0,0,0,0][0,0,0,0][0,0,0,0][0,0,0,0]]
  // [[0,0,0,1][0,0,1,1][0,1,1,1][0,0,1,1]]
  // [[0,0,0,0][0,0,0,1][0,0,1,1][0,0,0,1]]


  void calculateScors(){
    int baseScore = 100;

    // Calculate the line score
    int lineScore = linesCleared! * baseScore;

    // Calculate the time score (penalty of 2 points per second)
    int timePenaltyPerSecond = 2;
    int timeScore = max(0, lineScore - (playingTime * timePenaltyPerSecond));

    // Calculate the final score
    int finalScore = lineScore + timeScore;

    currentScors = finalScore;
  } // End of calculateScors method.


  bool validMove() {
    for (Tile tile in currentBlock.tiles) {
      if (tile.x >= width ||
          tile.x < 0 ||
          tile.y >= height ||
          tile.y < 0 ||
          // also if there is another bloack
          boardState![tile.x][tile.y] == 1) {
        return false;
      }
    }
    return true;
  } // End of validMove method.

  bool pieceMoving(String s) {
    bool pieceIsMoving = true;

    ctx!.fillStyle = 'gray'; // Fill the board itself with gray color.

    // when the tile move down on the screen, we actual draw the tile then deleted then draw the tile then deleted and so on..
    currentBlock.tiles.forEach((Tile tile) {
      ctx!.fillRect(
        tile.x * cellSize,
        tile.y * cellSize,
        cellSize,
        cellSize,
      );
      // Add a 2-pixel gray border around the Tile to over the black 1-pixel
      ctx!.strokeStyle = 'gray';
      ctx!.lineWidth = 2;
      ctx!.strokeRect(tile.x * cellSize, tile.y * cellSize, cellSize, cellSize);
    });
    

    if (s == 'rotate') {  // when user hit up kay
      currentBlock.rotateRight();
    } else {
      currentBlock.move(s);  // otherwise move the tile in the direction s
    }

    // the reaction to an invalid move will be move in the apposite direction.
    if (!(pieceIsMoving = validMove())) {
      if (s == 'rotate') currentBlock.rotateLeft();
      if (s == 'left') currentBlock.move('right');
      if (s == 'right') currentBlock.move('left');
      if (s == 'down') currentBlock.move('up');
      if (s == 'up') currentBlock.move('down');
    }

    
    // render the pice into the screen
    ctx!.fillStyle = currentBlock.color;

    currentBlock.tiles.forEach((tile) {
      ctx!.fillRect(
        tile.x * cellSize,
        tile.y * cellSize,
        cellSize,
        cellSize,
      );
      // Add a 1-pixel border around the Tile
      ctx!.strokeStyle = 'black';
      ctx!.lineWidth = 1;
      ctx!.strokeRect(tile.x * cellSize, tile.y * cellSize, cellSize, cellSize);
    });

    return pieceIsMoving;
  } // End of pieceMoving method.

  void updateGame(Timer timer) {
    window.console.log(boardState);
    window.console.log(rowState);

    // setup and show information panel
    if(querySelector('.info-container') == null){
      gameScore.setInnerHtml(
        '''
        <div class='info-container'>
          <div class='info-success'>Name: ${View.username}</div>
          <div class='info-success'>Level: $level</div>
          <div class='info-warning'>Cleared Lines: $linesCleared</div>
          <div class='info-success'>Level Scors: $currentScors</div>
          <div class='info-danger'>High Scors: ${currentScors! > View.scors ? currentScors : View.scors}</div>
        </div>
        ''',
      );
    }else{
      // Refresh the content of the .info-container div
      DivElement infoContainer = querySelector('.info-container') as DivElement;
      infoContainer.setInnerHtml('''
        <div class='info-success'>Name: ${View.username}</div>
        <div class='info-success'>Level: $level</div>
        <div class='info-warning'>Cleared Lines: $linesCleared</div>
        <div class='info-success'>Level Scors: $currentScors</div>
        <div class='info-danger'>High Scors: ${currentScors! > View.scors ? currentScors : View.scors}</div>
      ''');
      infoContainer.style.display = 'block';
    }


    // if the pice is not moving down(the user is interacting or the pice arrived to button of the screen)
    if (!pieceMoving('down')) {

      // then shoud update the board state to be (1) in the tiles positions and increment rowState
      currentBlock.tiles.forEach((t) {
        boardState![t.x][t.y] = 1;
        rowState![t.y]++;
      });

      clearRows(); // Clear Full rows.

      currentBlock = getRandomPiece(); // generate new random block

      if (!pieceMoving('down')) { // if true : meaning the new generated block can't moving down, the board is fully.
        print("the end"); // end of the game
        timer.cancel(); // stop the game
        playingTimer.cancel(); // stop the timer

        querySelector('#timer')!.style.display = 'none'; // hide the timer container

        board.style.display = 'none'; // hide the main board

        // setup and show information panel
        if(querySelector('.score-container') == null){
          gameScore.setInnerHtml(
            '''
            <div class="score-container">
              <h2 class="username">Name: ${View.username}</h2>
              <h2 class="cleared-lines">Cleared Lines: $linesCleared</h2>
              <h2 class="high-score">High Score: ${currentScors! > View.scors ? currentScors : View.scors}</h2>
              <h2 class="game-end">THE END</h2>
              <div class="button-container">
                <button class="delete-account-btn">Delete Account</button>
                <button class="reset-account-btn">Reset Account</button>
                <button class="restart-game-btn">Restart Game</button>
              </div>
            </div>

            ''',
          );
        }else{
          // Refresh the content of the .info-container div
          DivElement scoreContainer = querySelector('.score-container') as DivElement;
          scoreContainer.setInnerHtml('''
            <h2 class="username">Name: ${View.username}</h2>
              <h2 class="cleared-lines">Cleared Lines: $linesCleared</h2>
              <h2 class="high-score">High Score: ${currentScors! > View.scors ? currentScors : View.scors}</h2>
              <h2 class="game-end">THE END</h2>
              <div class="button-container">
                <button class="delete-account-btn">Delete Account</button>
                <button class="reset-account-btn">Reset Account</button>
                <button class="restart-game-btn">Restart Game</button>
              </div>
          ''');
          querySelector('.info-container')!.style.display = 'none';
          querySelector('.score-container')!.style.display = 'block';
        }
        
        
      // set scors and go to end section
      View.endTheGame(currentScors!);

      }
    }
  } // End of updateGame method.



  void initializeCanvas() {
    board = CanvasElement(); 
    board.width = width * cellSize;
    board.height = height * cellSize;
    ctx = board.context2D;

    ctx!.fillStyle = 'grey';
    ctx!.fillRect(0, 0, board.width!, board.height!);

    // Add a 1-pixel border around the board
    // ctx!.strokeStyle = 'black';
    // ctx!.lineWidth = 1;
    // ctx!.strokeRect(0, 0, board.width!, board.height!);

  } // End of initializeCanvas method.

  void handleKeyboard(Timer timer) {
    document.onKeyDown.listen((event) {

      if (timer.isActive) { // check if the game is still running
        if (event.keyCode == 37) pieceMoving('left'); // left arrow

        if (event.keyCode == 38) pieceMoving('rotate'); // up arrow

        if (event.keyCode == 39) pieceMoving('right'); // right arrow

        if (event.keyCode == 40) pieceMoving('down'); // down arrow

        if (event.keyCode == 32) while (pieceMoving('down')) {} // space bar
      }
    });
  } // End of handleKeyboard method.


  void displayTimer(int seconds) {

    DivElement timerDisplay = querySelector('#timer') as DivElement;

    timerDisplay.style.display = 'block'; // show the timer container if hidden

    // Calculate minutes and remaining seconds
    int minutes = seconds ~/ 60;
    int remainingSeconds = seconds % 60;

    // Format the time to display with leading zeros
    String formattedTime = '$minutes:${remainingSeconds.toString().padLeft(2, '0')}';

    timerDisplay.text = 'Time: $formattedTime';

  } // End of updateTimerDisplay method.



  void start(String selectedLevel) {
    level = selectedLevel;
    int duration = 1000;
    if(level == 'Easy'){ duration = 1000; }
    if(level == 'Medium'){ duration = 500; }
    if(level == 'Hard'){ duration = 250; }

    initializeCanvas();

    querySelector('#level')!.style.display = 'none';

    Element? info = querySelector('#info');

    Element? entryPoint = querySelector('#output');

    info!.append(gameScore);
    entryPoint!.nodes.add(board);

    // Start the timer
    playingTime = 0;
    playingTimer = Timer.periodic(Duration(seconds: 1), (_) {
      displayTimer(++playingTime);
    });
    
    // setup running Timer
    Timer timer = Timer.periodic(
      Duration(milliseconds: duration),
      updateGame,
    );

    currentBlock = getRandomPiece();
    handleKeyboard(timer);
  } // End of start method.


} // End the Game Controller
