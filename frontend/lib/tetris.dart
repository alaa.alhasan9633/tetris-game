library tetris;

import 'dart:html';
import 'dart:async';
import 'dart:math';

import 'package:tetris/src/view/view.dart';


part 'src/model/tile.dart';
part 'src/model/block.dart';
part 'src/model/blocks.dart';
part 'src/controller/game.dart';
