from flask import Flask, request, jsonify
from flask_pymongo import PyMongo
from bson import ObjectId
from flask_cors import CORS  # Import the CORS module

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://localhost:27017/tetrisgame"   # database on local machine
# app.config["MONGO_URI"] = "mongodb+srv://alaaalhasan9633:mongodbpass@cluster0.meqayqr.mongodb.net/tetrisgame?retryWrites=true&w=majority"

mongo = PyMongo(app)

# Use CORS with default configuration (allow all origins)
CORS(app, origins=["http://127.0.0.1:8080"])
# CORS(app, origins=["http://localhost"])

#-------------------- Home Root-----------------------------
@app.route("/")
def home():
    return "Welcome Home!"



#-------------------- get all users -----------------------------------------
@app.route("/get_all_users", methods=["GET"])
def get_all_users():
    users = mongo.db.users.find({})
    user_list = []
    for user in users:
        user_list.append({
            "name": user.get("name"),
            "email": user.get("email"),
            "scors": user.get("scors")
        })
    return jsonify(user_list)




#--------------------- add new user or register ---------------------------------
@app.route("/add_user", methods=["POST"])
def add_new_user():
    user_data = request.json  # Assumes you are sending JSON data in the request body
    name = user_data.get("name")
    email = user_data.get("email")
    password = user_data.get("pwd")
    scores = user_data.get("scors", 0)  # Set default scores to 0 if not provided

    # Insert the new user into the database and retrieve the generated ID
    user_id = mongo.db.users.insert_one({"name": name, "email": email, "pwd": password, "scors": scores}).inserted_id

    # Return the user ID in the response
    return jsonify({"message": "User added successfully", "user_id": str(user_id)})




#----------------- update scors for selected user ---------------------------------------
@app.route("/users/<user_id>", methods=["PUT"])
def update_user_scores(user_id):
    user_data = request.json  # Assumes you are sending JSON data in the request body
    scors = user_data.get("scors")

    # Update the scores field for the selected user
    mongo.db.users.update_one({"_id": ObjectId(user_id)}, {"$set": {"scors": scors}})

    return jsonify({"message": "User scors updated successfully"})



#--------------- delete user --------------------------------------
@app.route("/users/<user_id>", methods=["DELETE"])
def delete_user(user_id):
    # Delete the user with the given ID
    result = mongo.db.users.delete_one({"_id": ObjectId(user_id)})

    if result.deleted_count == 1:
        return jsonify({"message": "User deleted successfully"})
    else:
        return jsonify({"message": "User not found"}), 404



#------------------ login check if user exist -----------------------------------
@app.route("/login", methods=["POST"])
def login():
    login_data = request.json  # Assumes you are sending JSON data in the request body
    email = login_data.get("email")
    password = login_data.get("pwd")

    # Check if the user exists based on the provided email and password
    user = mongo.db.users.find_one({"email": email, "pwd": password})

    if user:
        user_data = {
            "id": str(user.get("_id")),  # Convert ObjectId to string for JSON serialization
            "name": user.get("name"),
            "email": user.get("email"),
            "scors": user.get("scors"),
        }
        return jsonify(user_data)
    else:
        return jsonify({"message": "Invalid email or password"}), 401
    



#------------------------- main -------------------------
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
